import 'package:flutter/material.dart';
import 'package:flutter_midtrans_wrapper/flutter_midtrans_wrapper.dart';

void main() => runApp(new MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final flutterWrapper = FlutterMidtransWrapper();

  @override
  void initState() {
    super.initState();
    _initSdk();
  }

  void _initSdk() {
    flutterWrapper.onTransactionFinished = (MidtransTransactionStatus status) {
      debugPrint('DART: TRX FINISHED');
      debugPrint('TRX FINISHED with status: $status');

      switch (status) {
        case MidtransTransactionStatus.Success:
          debugPrint('Transaction success!');
          break;
        case MidtransTransactionStatus.Pending:
          debugPrint('Transaction status pending');
          break;
        case MidtransTransactionStatus.Invalid:
          debugPrint('Transaction Invalid');
          break;
        case MidtransTransactionStatus.Failed:
          debugPrint('Payment Failed');
          break;
        case MidtransTransactionStatus.Cancelled:
          debugPrint('User cancelling payment flow');
          break;
      }
    };

    flutterWrapper.initMidtransSdk(
        'SB-Mid-client-Zjje6KmnmJRmLYwi', 'https://www.santika.com',
        isSandbox: true);
  }

  void _setUserDetail() {
    var userDetail = UserDetailMidtrans()
      ..userFullName = 'Tannama'
      ..email = 'tannama@oval.id'
      ..phoneNumber = '080989999'
      ..address = 'Menara Kompas, Palmerah, Jakarta Barat'
      ..city = 'Jakarta'
      ..zipCode = '12345'
      ..country = 'Indonesia';
    flutterWrapper.setUserDetail(userDetail);
  }

  void _addItemDetail() {
    var itemDetail = ItemDetailMidtrans()
      ..id = '123'
      ..price = 20000.0
      ..quantity = 2
      ..name = 'Dummy Item';
    flutterWrapper.addItemDetail(itemDetail);
  }

  void _createTransaction() {
    flutterWrapper.setTransactionRequest('TRX123456789', 40000.0);
  }

  void _startPayment() {
    flutterWrapper.startPayment();
  }

  void _startPaymentWithUserDetail() {
    flutterWrapper.startPayment(skipCustomerDetails: false);
  }

  var snapToken = '34df554e-5250-4b37-b2f9-3c213a398007';

  void _startPaymentWithToken() async {
    await flutterWrapper.startPayment(snapToken: snapToken);
  }

  void _startBankTransferPaymentWithToken() async {
    await flutterWrapper.startPayment(
        paymentMethod: MidtransPaymentMethod.BankTransferPermata,
        snapToken: snapToken);
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
        appBar: new AppBar(
          title: const Text('Plugin example app'),
        ),
        body: new Center(
          child: Column(
            children: <Widget>[
              /// Temporary disabled
              // RaisedButton(
              //   child: Text('Set User Detail'),
              //   onPressed: _setUserDetail,
              // ),
              // RaisedButton(
              //   child: Text('Add Item Detail'),
              //   onPressed: _addItemDetail,
              // ),
              // RaisedButton(
              //   child: Text('Create Transaction'),
              //   onPressed: _createTransaction,
              // ),
              // RaisedButton(
              //   child: Text('Start Payment'),
              //   onPressed: _startPayment,
              // ),
              // RaisedButton(
              //   child: Text('Start Payment With User Detail'),
              //   onPressed: _startPaymentWithUserDetail,
              // ),
              RaisedButton(
                child: Text('Start SNAP Payment'),
                onPressed: _startPaymentWithToken,
              ),
              RaisedButton(
                child: Text('Start SNAP Payment Direct Bank'),
                onPressed: _startBankTransferPaymentWithToken,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

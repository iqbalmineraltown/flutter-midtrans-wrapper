# flutter_midtrans_wrapper

A new Flutter plugin.

## Getting Started

For help getting started with Flutter, view our online
[documentation](https://flutter.io/).

For help on editing plugin code, view the [documentation](https://flutter.io/developing-packages/#edit-plugin-package).


## How To Integrate

### Setup Android Project

On `AndroidManifest.xml`
- add `xmlns:tools="http://schemas.android.com/tools"` on `<manifest>`
- add `tools:replace="android:label"` and `android:theme="@style/AppTheme"` on `<application>`

On `styles.xml`
- add following snippet under `<resources>`
```
    <style name="AppTheme" parent="Theme.AppCompat.Light.DarkActionBar">
        <item name="windowNoTitle">true</item>
        <item name="windowActionBar">false</item>
    </style>
```

On `android/build.gradle`
- add following snippet under `subprojects`
```
    project.configurations.all {
        resolutionStrategy.eachDependency { details ->
            if (details.requested.group == 'com.android.support'
                    && !details.requested.name.contains('multidex')) {
                details.useVersion "27.1.1"
            }
        }
    }
```

## Setup iOS Project
Not implemented yet

## Usage on Flutter Project
Initialize SDK. Providing transaction finished callback is optional
```
...
import 'package:flutter_midtrans_wrapper/flutter_midtrans_wrapper.dart';
...

final _midtransMobileWrapper = FlutterMidtransWrapper();

void _initMidtrans() {
    _midtransMobileWrapper.onTransactionFinished =
        _onMidtransSnapTransactionFinished;
    _midtransMobileWrapper.initMidtransSdk("<merchant-client-key>", "<merchant-url>");
}

void _onMidtransSnapTransactionFinished(MidtransTransactionStatus status) {
    print("Transaction Finished with status");
  }

```

Then trigger UIKit by providing snap token
```
_midtransMobileWrapper.startPayment( "<snap-token>");
```
If you want to show one specific payment channel instruction:

```
_midtransMobileWrapper.startPayment(MidtransPaymentMethod.BankTransfer, "<snap-token>");
```



## Special Note
This plugin is optimized for using Midtrans SnapToken with UIKit

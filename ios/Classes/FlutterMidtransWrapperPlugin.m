#import "FlutterMidtransWrapperPlugin.h"
#import <flutter_midtrans_wrapper/flutter_midtrans_wrapper-Swift.h>

@implementation FlutterMidtransWrapperPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterMidtransWrapperPlugin registerWithRegistrar:registrar];
}
@end
